class Config {
    constructor() {
        this.config = {
            dev: {
                host: 'start-chat',
                port: '8080'
            },
            stage: {
                host: 'b2cv2.lsexstage.int',
                port: '8080'
            },
            prod: {
                host: 'b2cv2.labelsex.com',
                port: '8080'
            }
        }
    }

    byEnv(env) {
        return this.config[env] == undefined ? this.config['dev'] : this.config[env];
    }
}

module.exports = new Config;